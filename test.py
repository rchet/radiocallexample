#!/usr/bin/env python3
#
# Author    : Ryan Chetcuti
# Date      : 06/10/2019
# 
# This scripts runs and instance of the Radio Server API server
# and runs a number of basic tests against the code to ensure 
# the basic functionality of the radio server code. 
#
# Modules that might need instalation : 
#   pip install requests
#
# Example Command Line  : 
#   ./test.py 
# 
# Future improvements:
#   - Use a testing platform like unittest or pytest
#   - ensure complete test coverage 
#   - test for race conditions

import requests
import radio_server
from multiprocessing import Process
from time import sleep


class TestCase1(object):

    def __init__(self):
        self.keyword = 'Jing'
        # create an instance of the class
        self.radio_server = radio_server.RadioServer(self.keyword)
        # start the flask thread that runs the api service
        self.api_server = Process(target=radio_server.api.run)
        self.api_server.start()
        self.base_url = 'http://127.0.0.1:5000'

    def port_good_submission(self, number_to_send):
        response = requests.post(
            self.base_url + '/SubmitEntry',
            data={'keyword': self.keyword, 'number': number_to_send}
        )
        print(response.text)
        return response.json()

    def port_bad_submission(self, number_to_send):
        response = requests.post(
            self.base_url + '/SubmitEntry',
            data={'keyword': self.keyword+'bad', 'number': number_to_send}
        )
        print(response.text)
        return response.json()

    def get_status(self):
        response = requests.get(self.base_url + '/GetStatus')
        return response.json()

    def terminate(self):
        self.api_server.terminate()
        self.api_server.join()

    def run_tests(self):
        starting_data = self.get_status()
        current_result = int(starting_data['CurrentResult'])
        print("Staring point : %d" % current_result)

        # Send a good request
        self.port_good_submission(333)
        current_data = self.get_status()
        assert(int(current_data['CurrentResult']) - current_result == 333)
        assert(int(current_data['Requests']) == 1)
        current_result = int(current_data['CurrentResult'])

        # Send a bad request, make sure values don't change
        self.port_bad_submission(321)
        current_data = self.get_status()
        requests = 1
        assert(int(current_data['CurrentResult']) == current_result)
        assert(int(current_data['Requests']) == 1)

        # Loop until a winner is found.
        # check that the id of the winner is correct
        increment = 111
        while (current_result % 11):
            response_data = self.port_good_submission(increment)
            requests += 1
            # Get the status of the competition after each request
            current_data = self.get_status()
            response_result = int(current_data['CurrentResult'])
            # Check that the result has increased by the correct amount
            assert(response_result - current_result == increment)
            # Check that the number of requests have increased correctly
            assert(int(current_data['Requests']) == requests)
            current_result += increment
            # If this was a winning request, check that we are informed
            if not (current_result % 11):
                # Make sure the reply to submission informs user that she won
                assert(response_data['Winner'])
                # Make sure that the ID of the winner is recorded correcty
                assert(
                    current_data['CompetitionWinner'] == response_data['ID']
                )

        # Check that once the competition ends, no new submissions are accepted
        response_data = self.port_good_submission(111)
        current_data = self.get_status()
        assert(int(current_data['CurrentResult']) == current_result)
        assert(int(current_data['Requests']) == requests)
        assert("Competiton has already been won" == response_data)


if __name__ == '__main__':
    scenario_1 = TestCase1()
    sleep(3)  # Sleep until the service is fully up and running
    scenario_1.run_tests()
    print('All Tests Completed')
    scenario_1.terminate()  # Terminate the API service
