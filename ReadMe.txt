Answers to the questions of the challenge : 

 - Provide an example on how to run your solution
  ./radio_server.py -k 11 montypython 
    this will start a REST API server on http://127.0.0.1:5000 , expecting the 
    keyword 'montypython' and serving two API Points (refer further down this 
    document for  more info)

    ./test.py runs the tests on the server as well

 - Give specs of your solution, the time it took to develop it and how to improve it
       Time of develpment : 6 to 8 hours. 

       The solution can be greatly improved by having all REST endpoints that start 
and stop the competition, and a database on the backend to maintain the state 
instead of a file. 

        If there was a database at the backend, multiple instnaces of this node could be
run, all of them connecting to the same db. That would greatly increase the number
of requests that could be handled. 

       Since this is a competition, a lot of anti-cheating considerations should
       be put into place, like authentication to ensure that a user submits only 
       one request per competition, anti-DDOS mechanisms should be developed, and
       authentication and authorization for the request to start and monitor 
       the competiton.  

 - Discuss the fairness of the contest.

    It was assumed that only requests with the correct keyword contribute to 
    increasing the result. 

    Race Condition : a threading lock was used to avoid a race condition when 
    muplitple requests arrive a the same time. The result, number of requests 
    are only updated once the lock is aquired. This should make the system 'fair'
    in that all requests are processed 'nearly' on a first-come-first-serverd 
    basis, and it ensures only 1 winner. 

    The lock system does not gurantee a first-come-first-servered, since it is 
    up to the OS which thread gets how much processing time, so it is possible 
    for a subsequent request to get processed more and reach the lock request
    point before another thread. 

    The use of the factor (11) greatly influences for how long the competition 
    runs. The larger the factor, the lower probability that the multiple is
    encountered quickly. A value of 11 is usefule to test with, but it did give 
    winning requests in less that 10 attempts at times. 

===============================================================================
Files in this project : 

- radio_server.py  - starts a REST APi server that runs the competition
- test.py          - Starts a radio_server instance, and runs a number of tests

The test.py was used during development to test the code and ensure the 
functionality was provided, however there is still a lot of use cases that are 
missing, and code coverage is far from complete. 

===============================================================================
radio_server.py
===============

 This script starts an instance of a REST API server that runs a competition.
 The competition has a keyword, and a total result.
 People can send in submissions by sending a keyword and an intiger value.
 the integer value has to be 3 digits long
 If the submission has the correct keyword, the value is added to the current
 result, and if the result is divisible by the factor (default = 11) the 
 the submission is deemed a winning entry. 

 To prevent race conditions, a lock is used within the class, and only once
 the threading lock is obtained can the values be changed. This means that only
 one thread will be changing the values at one point in time. 

 -----------------------------------------------------------------------------

 Modules that would require pip instalation  : 
   pip install flask

 Command line exmpale : 
  ./radio_server.py -k 11 montypython 

   This should start a REST server listening on http://127.0.0.1:5000
 -----------------------------------------------------------------------------
 API Points: 
   -> POST request to  <base_url>/SubmitEntry
       data should be url encoded data, with the two values:
          'keyword' = <keyword that user wants to submit
          'number' =  <value that the person wants to submit
       returns a JSON structure : 

   -> GET request to <base_url>/GetStatus
       returns a JSON Structure with the details of the competition

===============================================================================
test.py
===============

 Modules that might need instalation : 
   pip install requests

 Example Command Line  : 
   ./test.py 
 