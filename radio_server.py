#!/usr/bin/env python3
#
# Author    : Ryan Chetcuti
# Date      : 06/10/2019
# 
# This script starts an instance of a REST API server that runs a competition.
# The competition has a keyword, and a total result.
# People can send in submissions by sending a keyword and an intiger value.
# the integer value has to be 3 digits long
# If the submission has the correct keyword, the value is added to the current
# result, and if the result is divisible by the factor (default = 11) the 
# the submission is deemed a winning entry. 
#
# -----------------------------------------------------------------------------
#
# Modules that would require pip instalation  : 
#   pip install flask
#
#
# Command line exmpale : 
#  ./radio_server.py -k 11 montypython 
#
#   This should start a REST server listening on http://127.0.0.1:5000
#
# -----------------------------------------------------------------------------
# API Points: 
#   -> POST request to  <base_url>/SubmitEntry
#       data should be url encoded data, with the two values:
#          'keyword' = <keyword that user wants to submit
#          'number' =  <value that the person wants to submit
#       returns a JSON structure  
#
#   -> GET request to <base_url>/GetStatus
#       returns a JSON Structure with the details of the competition
# -----------------------------------------------------------------------------
 
# Future Improvements : 
#       - Fix the API Endpoints to accept JSON format in the request
#       - Start and stop the competition from REST API requests
#       - Save the state to a database rather than a file 
#       - Recored each and every request to database for posterity
#       - Use a better system of generating an ID


import random
from flask import Flask, json, request, Response
from sys import argv
import threading


help = '''
Radio Server starts a competition with keyword as the commmand line argument.

%s  [-k COMPETITION_FACTOR ] [-l] KEYWORD

    -l          indicates that the server should load data from a saved file.
                Filename is the same as the keyword and this should be used to
                recover from a previous crash.

    -k          integer for the factor used to divide the current result.
                defaults to 11.

    KEYWORD     string for the keyword expected in the request.
                requests that do not have the correct keyword are not counted.

''' % argv[0]

api = Flask(__name__)


class RadioServer(object):
    instance = None     # Assuming there is always one instance at a time
    # used during testing to spin up and tear down servers

    @staticmethod
    def load_server(saved_filename):
        # Load all the info form a file and return a server instance
        saved_file = open(saved_filename)
        last_line = saved_file.readlines()[-1]
        arguments = last_line.split(',')
        temp_server = RadioServer(arguments[2], int(arguments[3]))
        temp_server.TotalReq = int(arguments[0])
        temp_server.result = int(arguments[1])
        if 'True' in arguments[4]:
            temp_server.CompWon = True
        temp_server.CompWinner = int(arguments[5])
        return temp_server

    def __init__(self, comp_keyword, comp_factor=11, filename=None):
        # comp_keyword = the keyword used for the compettion
        # comp_factor = factor by which a value is test to find winner
        # filename = the name of the file to which to write the status
        if not filename:
            filename = comp_keyword  # if no filename supplied, use the keyword
        self._comp_keyword = comp_keyword
        self._result = random.randint(100, 999)
        self._total_req = 0
        self._comp_winner = 0    # This will store the ID of the winner
        self._comp_factor = comp_factor
        self._comp_won = False  # indicates whether the competition was won
        self._write_file = open(filename, 'a')
        self._lock = threading.Lock()
        RadioServer.instance = self  # Horrible thing, quick workaround
        # to work with Flask
        print(
            "Started server with keyword '%s',"
            " result '%d' and a selection factor %d"
            % (self._comp_keyword, self._result, self._comp_factor)
        )

    def print_state(self, req_keyword, req_value, req_id):
        return {
            'ID': req_id,
            'KeywordReceived': req_keyword,
            'ValueReceived': req_value,
            'CurrentResult': self._result
        }

    def save_state(self, req_id=0):
        self._write_file.write(
            "%d,%d,%s,%d,%s,%d\n"
            % (
                self._total_req,
                self._result,
                self._comp_keyword,
                self._comp_factor,
                self._comp_won,
                req_id
            )
        )
        self._write_file.flush()

    def add_digits(self, req_value, req_id):
        # returns true if the result is divisible by the factor
        # is thread safe by using a lock for any changes to variables
        assert(type(req_value) == int)
        assert(type(req_id) == int)
        assert(req_value < 1000)
        assert(req_value > 99)
        with self._lock:  # Get the lock before changing shared var
            self._result += req_value
            self._total_req += 1
            if not (self._result % self._comp_factor):
                self._comp_won = True  # Mark the competition as finished
                self._comp_winner = req_id
                self.save_state(req_id)
                return True
            else:
                self.save_state(req_id)
                return False

    def get_comp_status(self):
        return {
            'Requests': self._total_req,
            'CurrentResult': self._result,
            'Won': self._comp_won,
            'Keyword': self._comp_keyword,
            'CompetitionFactor': self._comp_factor,
            'CompetitionWinner': self._comp_winner
        }

    def has_been_won(self):  # returns true if the competition was won
        return self._comp_won

    def process_req(self, req_keyword, req_value, req_id):
        if not self._comp_won:
            if (self._comp_keyword == req_keyword):
                if self.add_digits(req_value, req_id):
                    return {
                        'ID': req_id,
                        'KeywordReceived': req_keyword,
                        'ValueReceived': req_value,
                        'CurrentResult': self._result,
                        'Winner': True
                    }
            self.save_state(req_id)
            return self.print_state(req_keyword, req_value, req_id)
        else:
            return "Competiton has already been won"

    @api.route('/SubmitEntry', methods=['POST'])
    def submit_req():
        keyword = request.form.get('keyword')
        number = int(request.form.get('number'))
        if (number) and (keyword):
            remote_port = str(request.environ.get('REMOTE_PORT'))
            remote_addr = request.remote_addr
            request_id = hash(remote_port + remote_addr)
            response_data = json.dumps(
                RadioServer.instance.process_req(keyword, number, request_id)
            )
            return Response(response_data, mimetype='application/json')
        else:
            return Response(
                json.dumps('Something was wrong in your request'),
                mimetype='application/json'
            )

    @api.route('/GetStatus', methods=['GET'])
    def get_status():
        response_data = json.dumps(RadioServer.instance.get_comp_status())
        return Response(response_data, mimetype='application/json')


def santize_int(int_input):
    # Should check that is truly and integer here
    # before using the 'int()' function on it
    return int(int_input)


if __name__ == '__main__':
    if (len(argv) > 1):
        print(argv)
        try:
            fileName = keyword = argv[-1]
            if ('-l' in argv):
                radio_server = RadioServer.load_server(fileName)
            else:
                if('-k' in argv):
                    factor = santize_int(argv[argv.index('-k')+1])
                radio_server = RadioServer(
                    comp_keyword=keyword,
                    filename=fileName,
                    comp_factor=factor
                )
            api.run()
        except Exception as problem:
            print(problem)
            print(help)
    else:
        print(help)
